﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject Zombie;
    public GameObject StrangeThing;
    public Animator Animator;

    private bool Helicop;
    private float CurrentTime;

    private void Awake()
    {
        Zombie.SetActive(true);
        Animator = Zombie.GetComponent<Animator>();
        Animator.enabled = false;
        Helicop = false;
        CurrentTime = Time.time;
    }

    private void Update()
    {
        if (Time.time >= CurrentTime + 1.5f)
        {
            if (transform.position.z < Zombie.transform.position.z + 1.5f && !Helicop)
            {
                transform.position += transform.forward * Time.deltaTime * 6;
                if (transform.position.z > Zombie.transform.position.z && !Animator.enabled)
                {
                    Animator.enabled = true;
                }
            }
            else
            {
                if (transform.eulerAngles.y < 180)
                {
                    transform.Rotate(0, Time.deltaTime * 120, 0);
                }
                else
                {
                    Helicop = true;
                    StrangeThing.SetActive(true);
                }
            }
            if (Helicop)
            {
                StrangeThing.transform.RotateAroundLocal(Vector3.up, Time.deltaTime * 120);
            }
        }
    }
}